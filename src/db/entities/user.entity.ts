import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({ name: 'USER' })
export class User extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'ID' })
  id: number;

  @Column({ name: 'NAME', type: 'varchar', nullable: false })
  name: string;

  @Column({ name: 'PASSWORD', type: 'varchar' })
  password: string;

  @Column({ name: 'EMAIL', type: 'varchar' })
  email: string;

  @Column({ name: 'CREATED_AT' })
  createdAt: Date;

  @Column({ name: 'DELETED_AT' })
  deletedAt: Date;
}
