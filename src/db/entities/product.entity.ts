import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({ name: 'PRODUCT' })
export class Product extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'ID' })
  id: number;

  @Column({ name: 'NAME' })
  name: string;

  @Column({ name: 'PRICE' })
  price: number;

  @Column({ name: 'CREATED_AT' })
  createdAt: Date;

  @Column({ name: 'DELETED_AT' })
  deletedAt: Date;                              
}
