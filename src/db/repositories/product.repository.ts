import { Product } from '../entities/product.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Product)
export class ProductRepository extends Repository<Product> {
  async createProduct(product: Product): Promise<Product> {
    return await Product.save(product);
  }

  async getProduct(productId: number): Promise<Product> {
    return await Product.createQueryBuilder('product')
      .where('product.id = :productId', { productId })
      .andWhere('product.deletedAt IS NULL')
      .getOne();
  }

  async getProducts(): Promise<Product[]> {
    return await Product.createQueryBuilder('products').getMany();
  }

  async updateProduct(product: Product): Promise<Product> {
    return await Product.save(product);
  }

  async deleteProduct(product: Product): Promise<void> {
    product.deletedAt = new Date();
    await Product.save(product);
  }
}
