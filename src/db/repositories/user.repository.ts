import { User } from '../entities/user.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async createUser(user: User): Promise<User> {
    return await User.save(user);
  }

  async getUser(userId: number): Promise<User> {
    return await User.createQueryBuilder('user')
      .where('user.id = :userId', { userId })
      .andWhere('user.deletedAt IS NULL')
      .getOne();
  }

  async getByEmail(email: string): Promise<User> {
    return await User.createQueryBuilder('user')
      .where('user.email = :email', { email })
      .getOne();
  }

  async getUsers(): Promise<User[]> {
    return await User.createQueryBuilder('user').getMany();
  }

  async updateUser(user: User): Promise<User> {
    return await User.save(user);
  }

  async deleteUser(user: User): Promise<void> {
    user.deletedAt = new Date();
    await User.save(user);
  }
}
