import {
  Injectable,
  NotFoundException,
  ConflictException,
  UnauthorizedException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { User } from 'src/db/entities/user.entity';
import { SignUpUserDto } from './dtos/user-signup.dto';
import { UserRepository } from 'src/db/repositories/user.repository';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { ConfigService } from '@nestjs/config';
import { UserSignUpResponseDto } from './dtos/user-signup-response.dto';
import { LoginResponseDto } from './dtos/login-response.dto';

@Injectable()
export class AuthService {
  secret: string;

  constructor(
    private userRepository: UserRepository,
    private configService: ConfigService,
  ) {
    this.secret = configService.get<string>('secret');
  }

  async createUser(data: SignUpUserDto): Promise<UserSignUpResponseDto> {
    const findU = await this.userRepository.getByEmail(data.email);
    if (findU)
      throw new ConflictException(
        `El usuario con el email ${data.email} ya existe`,
      );

    const user = new User();
    user.email = data.email;
    user.name = data.name;
    user.password = await bcrypt.hash(data.password, 10);
    user.createdAt = new Date();

    const res = await this.userRepository.createUser(user);
    return new UserSignUpResponseDto({
      message: 'Usuario Registrado',
      data: {
        name: res.name,
        email: res.email,
        createdAt: res.createdAt,
      },
    });
  }

  async validateUser(payload: JwtPayload): Promise<User> {
    const user = await this.userRepository.getByEmail(payload.email);
    if (!user || (user && user.deletedAt))
      throw new NotFoundException(
        `user with email ${payload.email} not found`,
      );

    return user;
  }

  async login(payload: JwtPayload): Promise<LoginResponseDto> {
    const user = await this.userRepository.getByEmail(payload.email);
    if (!user)
      throw new UnauthorizedException(
        `Correo / Contraseña incorrecta`,
      );

    if (!(await bcrypt.compare(payload.password, user.password)))
      throw new UnauthorizedException('Correo / Contraseña incorrecta');

    const expiresIn = '1h';
    const accessToken = jwt.sign(payload, this.secret, { expiresIn });

    return new LoginResponseDto({
      message: 'Bienvenido',
      token: accessToken,
    });
  }
}
