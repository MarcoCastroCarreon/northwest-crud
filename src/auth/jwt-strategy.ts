import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import * as bcrypt from 'bcrypt';
import { UnauthorizedException, Inject } from '@nestjs/common';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserRepository } from 'src/db/repositories/user.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/db/entities/user.entity';
import { ConfigService } from '@nestjs/config';

export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    @InjectRepository(User) private userRepository: UserRepository,
    @Inject(ConfigService) private configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get<string>('secret'),
    });
  }

  async validate(payload: JwtPayload) {
    const user = await this.userRepository.getByEmail(payload.email);
    if (!user || (user && user.deletedAt)) throw new UnauthorizedException(`Invalid email`);

    await bcrypt.compare(payload.password, user.password, (err) => {
      if (err) throw new UnauthorizedException(`Wrong Password`);
    });

    return { id: user.id, email: user.email };
  }
}
