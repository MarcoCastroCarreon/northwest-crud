import { ApiProperty } from '@nestjs/swagger';

export class UserSignUpResponseDto {
  @ApiProperty({ type: String })
  message: string;

  @ApiProperty({
    type: Object,
    example: {
      name: 'string',
      email: 'string',
      createdAt: new Date(),
    },
  })
  data: {
    name: string;
    email: string;
    password: string;
    createdAt: Date;
  };

  constructor(data: any) {
    this.message = data.message;
    this.data = data.data;
  }
}
