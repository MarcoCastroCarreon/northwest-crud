import { JwtPayload } from "../interfaces/jwt-payload.interface";
import { ApiProperty } from "@nestjs/swagger";


export class LoginDto implements JwtPayload {
    @ApiProperty({ type: String, required: true })
    email: string;

    @ApiProperty({ type: String, required: true })
    password: string;

    constructor(payload: JwtPayload) {
        this.email = payload.email;
        this.password = payload.password;
    }
}