import { ApiProperty } from "@nestjs/swagger";


export class LoginResponseDto {
    @ApiProperty({ type: String })
    message: string;

    @ApiProperty({ type: String })
    token: string;

    constructor(data: any) {
        this.message = data.message;
        this.token = data.token;
    }
}