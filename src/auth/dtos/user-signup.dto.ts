import { UserSignUp } from "../interfaces/user-signup.interface";
import { ApiProperty } from "@nestjs/swagger";


export class SignUpUserDto implements UserSignUp {
    @ApiProperty({ type: String, required: true })
    name: string;

    @ApiProperty({ type: String, required: true })
    email: string;

    @ApiProperty({ type: String, required: true })
    password: string;

    constructor(data: UserSignUp) {
        this.name = data.name;
        this.email = data.email;
        this.password = data.password;
    }
}