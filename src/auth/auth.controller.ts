import {
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Body,
  BadRequestException,
} from '@nestjs/common';
import { ApiTags, ApiBody, ApiResponse } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { SignUpUserDto } from './dtos/user-signup.dto';
import { UserSignUp } from './interfaces/user-signup.interface';
import { LoginDto } from './dtos/login.dto';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserSignUpResponseDto } from './dtos/user-signup-response.dto';
import { Validation } from 'src/common/validation';
import { LoginResponseDto } from './dtos/login-response.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  @HttpCode(HttpStatus.CREATED)
  @ApiBody({ type: SignUpUserDto })
  @ApiResponse({ type: UserSignUpResponseDto, status: HttpStatus.CREATED })
  async signUp(@Body() userData: UserSignUp): Promise<UserSignUpResponseDto> {
    if (
      !userData.password ||
      userData.password.length < 6 ||
      userData.password == ''
    )
      throw new BadRequestException('cSecret debe tener al menos 6 caracteres');

    if (!(await Validation.validateEmail(userData.email)))
      throw new BadRequestException('email no tiene el formato válido');

    if (
      !userData.email ||
      userData.email.length < 3 ||
      !userData.name ||
      userData.name.length < 3
    )
      throw new BadRequestException(
        'Nombre, Email y Contraseña son requeridos',
      );
    return this.authService.createUser(userData);
  }

  @Post('/login')
  @HttpCode(HttpStatus.OK)
  @ApiBody({ type: LoginDto })
  @ApiResponse({ type: LoginResponseDto, status: HttpStatus.OK })
  async login(@Body() payload: JwtPayload): Promise<LoginResponseDto> {
    if (!payload.email || !(await Validation.validateEmail(payload.email)))
      throw new BadRequestException('email es necesario');
    if (!payload.password || payload.password.length < 6)
      throw new BadRequestException(
        'contraseña debe tener al menos 6 caracteres',
      );
    return this.authService.login(payload);
  }
}
