import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserRepository } from 'src/db/repositories/user.repository';
import { config } from 'dotenv';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt-strategy';
import { ConfigModule } from '@nestjs/config';

config();

const secret = process.env.JWT_SECRET;

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({ secret, signOptions: { expiresIn: '1h' } }),
    ConfigModule
  ],
  controllers: [AuthController],
  providers: [AuthService, UserRepository, LocalStrategy, JwtStrategy],
  exports: [TypeOrmModule, AuthService, JwtModule, JwtStrategy],
})
export class AuthModule {}
