import 'reflect-metadata';
import { User } from 'src/db/entities/user.entity';
import { Product } from 'src/db/entities/product.entity';

export default (): any => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  secret: process.env.JWT_SECRET,
  db: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: [
      User,
      Product
    ],
  },
});
