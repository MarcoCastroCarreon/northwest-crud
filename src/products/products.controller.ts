import {
  Controller,
  UseGuards,
  Post,
  HttpCode,
  HttpStatus,
  Body,
  BadRequestException,
  Get,
  Param,
  Delete,
  Put,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateProductDto } from './dtos/create-product.dto';
import { CreateProductPayload, UpdateProductPayload } from './interfaces/product.interface';
import { CreateProductResponseDto } from './dtos/create-product-response.dto';
import { GetProductResponseDto } from './dtos/get-product-response.dto';
import { GetProductsResponseDto } from './dtos/get-products-response.dto';
import { UpdateProductDto } from './dtos/update-product.dto';

@ApiTags('Products')
@Controller('products')
export class ProductsController {
  constructor(private productService: ProductsService) {}

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiBody({ type: CreateProductDto })
  @ApiResponse({ type: CreateProductResponseDto, status: HttpStatus.CREATED })
  async createUser(
    @Body() productData: CreateProductPayload,
  ): Promise<CreateProductResponseDto> {
    if (!productData.name || productData.name.length < 3)
      throw new BadRequestException(
        'el nombre del producto debe ser de almenos 3 caracteres',
      );
    return this.productService.createProduct(productData);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ type: GetProductResponseDto, status: HttpStatus.OK })
  async getProduct(@Param('id') id: number): Promise<GetProductResponseDto> {
    if (!id) throw new BadRequestException('El id en el path es requerido');
    if (isNaN(+id))
      throw new BadRequestException('El id debe de ser un número');

    return await this.productService.getProductById(+id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ type: GetProductsResponseDto, status: HttpStatus.OK })
  async getProducts(): Promise<GetProductsResponseDto> {
    return await this.productService.getProducts();
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({ status: HttpStatus.NO_CONTENT })
  async deleteProduct(@Param('id') id: number): Promise<void> {
    if (!id) throw new BadRequestException('El id en el path es requerido');
    if (isNaN(+id))
      throw new BadRequestException('El id debe de ser un número');
    return await this.productService.deleteProductById(+id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiBody({ type: UpdateProductDto })
  @ApiResponse({ status: HttpStatus.NO_CONTENT })
  async updateProduct(@Param('id') id: number, @Body() payload: UpdateProductPayload): Promise<void> {
    if (!id) throw new BadRequestException('El id en el path es requerido');
    if (isNaN(+id))
      throw new BadRequestException('El id debe de ser un número');
    return await this.productService.updateProduct(+id, payload);
  }
}
