export interface ProductInterface {
    id?: number;
    name: string;
    price: number;
    createdAt: Date;
    deletedAt: Date | null;
}

export interface CreateProductPayload {
    name: string;
    price: number;
}

export interface UpdateProductPayload {
    name?: string;
    price?: number;
}