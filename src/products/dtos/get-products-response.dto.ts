import { ApiProperty } from '@nestjs/swagger';
import { ProductInterface } from '../interfaces/product.interface';
import { Product } from 'src/db/entities/product.entity';

export class GetProductsResponseDto {
  @ApiProperty({ type: String })
  message: string;

  @ApiProperty({
    type: Array,
    example: [
      {
        id: 0,
        name: 'RandomName',
        price: 0,
        createAt: '2020-05-25 20:00:36.0',
      },
    ],
  })
  data: ProductInterface[];

  constructor(message: string, products: Product[]) {
    this.message = message;
    this.data = products.map(p => ({
      id: +p.id,
      name: p.name,
      price: +p.price,
      createdAt: p.createdAt,
      deletedAt: p.deletedAt
    }));
  }
}
