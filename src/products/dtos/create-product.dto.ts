import { CreateProductPayload } from '../interfaces/product.interface';
import { ApiProperty } from '@nestjs/swagger';

export class CreateProductDto implements CreateProductPayload {
    @ApiProperty({ type: String })
    name: string;

    @ApiProperty({ type: Number })
    price: number;
}