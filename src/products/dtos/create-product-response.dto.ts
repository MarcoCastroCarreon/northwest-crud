import { ApiProperty } from '@nestjs/swagger';
import { ProductInterface } from '../interfaces/product.interface';

export class CreateProductResponseDto {
  @ApiProperty({ type: String })
  message: string;

  @ApiProperty({
    type: Object,
    example: {
      id: 0,
      name: 'RandomName',
      price: 0,
      createAt: '2020-05-25 20:00:36.0',
    },
  })
  data: ProductInterface;

  constructor(data: any) {
    this.message = data.message;
    this.data = data.data;
  }
}
