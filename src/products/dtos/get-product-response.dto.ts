import { ApiProperty } from '@nestjs/swagger';
import { ProductInterface } from '../interfaces/product.interface';

export class GetProductResponseDto {
  @ApiProperty({ type: String })
  message: string;

  @ApiProperty({
    type: Object,
    example: {
      id: 0,
      name: 'RandomName',
      price: 0,
      createdAt: new Date(),
    },
  })
  data: ProductInterface;

  constructor(data: any) {
    this.message = data.message;
    this.data = data.data;
  }
}
