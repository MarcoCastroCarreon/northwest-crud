import { UpdateProductPayload } from "../interfaces/product.interface";
import { ApiPropertyOptional } from "@nestjs/swagger";


export class UpdateProductDto implements UpdateProductPayload {
    @ApiPropertyOptional({ type: String })
    name?: string;

    @ApiPropertyOptional({ type: Number })
    price?: number;
}