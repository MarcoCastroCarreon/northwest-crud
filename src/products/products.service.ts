import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ProductRepository } from 'src/db/repositories/product.repository';
import {
  CreateProductPayload,
  UpdateProductPayload,
} from './interfaces/product.interface';
import { Product } from 'src/db/entities/product.entity';
import { CreateProductResponseDto } from './dtos/create-product-response.dto';
import { GetProductResponseDto } from './dtos/get-product-response.dto';
import { GetProductsResponseDto } from './dtos/get-products-response.dto';
import copyObject from 'src/common/utils/copy-object';

@Injectable()
export class ProductsService {
  constructor(private productRepository: ProductRepository) {}

  async createProduct(
    productData: CreateProductPayload,
  ): Promise<CreateProductResponseDto> {
    try {
      const product = new Product();
      product.name = productData.name;
      product.price = productData.price;
      product.createdAt = new Date();

      const prodRes = await this.productRepository.createProduct(product);

      return new CreateProductResponseDto({
        message: 'Producto Creado',
        data: {
          id: +prodRes.id,
          name: prodRes.name,
          price: +prodRes.price,
          createdAt: prodRes.createdAt,
        },
      });
    } catch (error) {
      throw new InternalServerErrorException(
        `Hubo un problema al crear el producto - e: ${error.message}`,
      );
    }
  }

  async getProductById(productId: number): Promise<any> {
    const product = await this.productRepository.getProduct(productId);
    if (!product) throw new NotFoundException('No se encontró el producto');
    try {
      return new GetProductResponseDto({
        message: 'Producto Encontrado',
        data: {
          id: +product.id,
          name: product.name,
          price: +product.price,
          createdAt: product.createdAt,
        },
      });
    } catch (error) {
      throw new InternalServerErrorException(
        `Hubo un problema al intentar obtener el producto - e: ${error.message}`,
      );
    }
  }

  async getProducts(): Promise<GetProductsResponseDto> {
    try {
      const products = await this.productRepository.getProducts();
      if (!products.length) return { message: 'No hay productos', data: [] };

      return new GetProductsResponseDto('Productos', products);
    } catch (error) {
      throw new InternalServerErrorException(
        `Algo salió mal al intentar obtener los produtos - e: ${error.message}`,
      );
    }
  }

  async deleteProductById(productId: number): Promise<void> {
    const product = await this.productRepository.getProduct(productId);
    if (!product) throw new NotFoundException('No se encontró el producto');
    try {
      await this.productRepository.deleteProduct(product);
    } catch (error) {
      throw new InternalServerErrorException(
        `Algo salió mal al intentar eliminar el producto - e: ${error.message}`,
      );
    }
  }

  async updateProduct(
    productId: number,
    data: UpdateProductPayload,
  ): Promise<void> {
    const product = await this.productRepository.getProduct(productId);
    if (!product) throw new NotFoundException('No se encontró el producto');

    const copy = await copyObject(product);
    try {
      product.name = data?.name || product.name;
      product.price = data?.price || product.price;

      await this.productRepository.updateProduct(product);
    } catch (error) {
      await this.productRepository.updateProduct(copy as Product);
      throw new InternalServerErrorException(
        `Algo salió mal al intentar actualizar el producto - e: ${error.message}`,
      );
    }
  }
}
