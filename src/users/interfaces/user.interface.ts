export interface UserInterface {
    id?: number;
    name: string;
    email: string;
    createdAt: Date;
    deletedAt: Date | null;
}

export interface CreateUserPayload {
    name: string;
    email: string;
    password: string;
}

export interface UpdateUserPayload {
    name?: string;
    email?: string;
    password?: string;
}