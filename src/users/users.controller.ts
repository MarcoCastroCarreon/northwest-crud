import {
  Controller,
  Get,
  HttpStatus,
  HttpCode,
  Post,
  Body,
  BadRequestException,
  Put,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiTags, ApiResponse, ApiBody } from '@nestjs/swagger';
import { GetUsersResponseDto } from './dtos/get-users-response.dto';
import { CreateUserDto } from './dtos/create-user.dto';
import {
  CreateUserPayload,
  UpdateUserPayload,
} from './interfaces/user.interface';
import { CreateUserResponseDto } from './dtos/create-user-response.dto';
import { UpdateUserDto } from './dtos/update-user.dto';
import { AuthGuard } from '@nestjs/passport';
import { Validation } from 'src/common/validation';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  async getUser(@Param('id') id: number): Promise<any> {
    if (!id) throw new BadRequestException('El id en el path es requerido');
    if (isNaN(+id))
      throw new BadRequestException('El id debe de ser un número');
    return this.usersService.getUserById(+id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ type: GetUsersResponseDto, status: HttpStatus.OK })
  async getUsers(): Promise<GetUsersResponseDto> {
    return await this.usersService.getAllUsers();
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiBody({ type: CreateUserDto })
  @ApiResponse({ type: CreateUserResponseDto, status: HttpStatus.CREATED })
  async createUser(
    @Body() userData: CreateUserPayload,
  ): Promise<CreateUserResponseDto> {
    if (
      !userData.password ||
      userData.password.length < 6 ||
      userData.password === ''
    )
      throw new BadRequestException(
        'password debe tener al menos 6 caracteres',
      );

    if (!(await Validation.validateEmail(userData.email)))
      throw new BadRequestException('email no tiene el formato válido');
    if (
      !userData.email ||
      userData.email.length < 3 ||
      !userData.name ||
      userData.name.length < 3
    )
      throw new BadRequestException(
        'Nombre, Email y Contraseña son requeridos',
      );
    return this.usersService.createUser(userData);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiBody({ type: UpdateUserDto })
  @ApiResponse({ status: HttpStatus.NO_CONTENT })
  async updateUser(
    @Param('id') userId: number,
    @Body() userData: UpdateUserPayload,
  ): Promise<void> {
    if (typeof +userId !== 'number')
      throw new BadRequestException(`El id del usuario debe ser un número`);
    if (!userId)
      throw new BadRequestException(
        'El id del usuario en el path es necesario',
      );
    return await this.usersService.updateUser(userId, userData);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({ status: HttpStatus.NO_CONTENT })
  async deleteUser(@Param('id') userId: number): Promise<void> {
    return this.usersService.deleteUser(userId);
  }
}
