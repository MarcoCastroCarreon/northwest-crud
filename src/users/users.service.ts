import {
  Injectable,
  InternalServerErrorException,
  ConflictException,
  NotFoundException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UserRepository } from 'src/db/repositories/user.repository';
import { GetUsersResponseDto } from './dtos/get-users-response.dto';
import { GetUserResponseDto } from './dtos/get-user-response.dto';
import {
  CreateUserPayload,
  UpdateUserPayload,
} from './interfaces/user.interface';
import { User } from 'src/db/entities/user.entity';
import { CreateUserResponseDto } from './dtos/create-user-response.dto';

@Injectable()
export class UsersService {
  constructor(private userRepository: UserRepository) {}

  async getAllUsers(): Promise<GetUsersResponseDto> {
    const users = await this.userRepository.getUsers();
    if (!users.length) return { message: "No hay usuarios", data: [] };
    return new GetUsersResponseDto("Usuarios", users);
  }

  async getUserById(userId: number): Promise<GetUserResponseDto> {
    const user = await this.userRepository.getUser(userId);
    if (!user || (user && user.deletedAt))
      throw new NotFoundException('Usuario no encontrado');

    return new GetUserResponseDto({
      message: 'Usuario Encontrado',
      data: {
        name: user.name,
        email: user.email,
        createdAt: user.createdAt,
        deletedAt: user.deletedAt,
      },
    });
  }

  async createUser(data: CreateUserPayload): Promise<CreateUserResponseDto> {
    const userFound = await this.userRepository.getByEmail(data.email);
    if (userFound && !userFound.deletedAt)
      throw new ConflictException(
        `El usuario con el email ${data.email} ya se encuentra registrado`,
      );
    try {
      const user = new User();
      user.name = data.name;
      user.email = data.email;
      user.password = await bcrypt.hash(data.password, 10);
      user.createdAt = new Date();

      const uRes = await this.userRepository.createUser(user);

      return new CreateUserResponseDto('Usuario Creado', {
        id: +uRes.id,
        name: uRes.name,
        email: uRes.email,
        createdAt: uRes.createdAt,
      });
    } catch (error) {
      throw new InternalServerErrorException(
        `Algo ocurrió al intentar guardar el usuario - e: ${error.message}`,
      );
    }
  }

  async updateUser(userId: number, data: UpdateUserPayload): Promise<void> {
    const user = await this.userRepository.getUser(userId);
    if (!user)
      throw new NotFoundException(
        `El usuario con el id ${userId} no fue encontrado`,
      );

    user.email = data?.email || user.email;
    user.name = data?.name || user.name;
    user.password = data?.password ? await bcrypt.hash(data.password, 10) : user.password;

    try {
      await this.userRepository.updateUser(user);
    } catch (error) {
      throw new InternalServerErrorException(
        `No se pudo actualizar el usuario - e: ${error.message}`,
      );
    }
  }

  async deleteUser(userId: number): Promise<void> {
    const user = await this.userRepository.getUser(userId);
    if (!user)
      throw new NotFoundException(
        `El usuario con el id ${userId} no fue encontrado`,
      );

    try {
      await this.userRepository.deleteUser(user);
    } catch (error) {
      throw new InternalServerErrorException(
        `No se pudo eliminar su usuario - e: ${error.message}`,
      );
    }
  }
}
