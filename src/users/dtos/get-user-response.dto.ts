import { UserInterface } from '../interfaces/user.interface';
import { ApiProperty } from '@nestjs/swagger';

export class GetUserResponseDto {
  @ApiProperty({ type: String })
  message: string;

  @ApiProperty({
    type: Object,
    example: {
      id: 0,
      email: 'example@mail.com',
      name: 'RandomName',
      createAt: '2020-05-25 20:00:36.0',
      deleteAt: '2020-05-25 20:00:36.0',
    },
  })
  data: UserInterface;

  constructor(response: any) {
    this.data = response.data;
    this.message = response.message;
  }
}
