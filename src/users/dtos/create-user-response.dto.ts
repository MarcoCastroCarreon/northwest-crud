import { ApiProperty } from '@nestjs/swagger';
import { UserInterface } from '../interfaces/user.interface';

export class CreateUserResponseDto {
  @ApiProperty({ type: String })
  message: string;

  @ApiProperty({
    type: Object,
    example: {
      id: 0,
      email: 'example@mail.com',
      name: 'RandomName',
      createAt: '2020-05-25 20:00:36.0',
    },
  })
  data: UserInterface;

  constructor(message: string, data: any) {
    this.message = message;
    this.data = data;
  }
}
