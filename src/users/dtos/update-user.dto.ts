import { UpdateUserPayload } from "../interfaces/user.interface";
import { ApiProperty } from "@nestjs/swagger";


export class UpdateUserDto implements UpdateUserPayload {
    @ApiProperty({ type: String, required: false })
    email?: string;

    @ApiProperty({ type: String, required: false })
    name?: string;

    @ApiProperty({ type: String, required: false })
    password?: string;
}