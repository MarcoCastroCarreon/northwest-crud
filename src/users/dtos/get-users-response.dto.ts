import { UserInterface } from '../interfaces/user.interface';
import { User } from 'src/db/entities/user.entity';
import { ApiProperty } from '@nestjs/swagger';

export class GetUsersResponseDto {
  @ApiProperty({ type: String })
  message: string;

  @ApiProperty({
    type: Array,
    example: [
      {
        id: 0,
        email: 'example@mail.com',
        name: 'RandomName',
        createAt: '2020-05-25 20:00:36.0',
        deleteAt: '2020-05-25 20:00:36.0',
      },
    ],
  })
  data: UserInterface[];

  constructor(message: string, us: User[]) {
    this.message = message;
    this.data = us.map(u => ({
      id: +u.id,
      email: u.email,
      name: u.name,
      createdAt: u.createdAt,
      deletedAt: u.deletedAt,
    }));
  }
}
