import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import config from 'src/config/configuration';
import { PassportModule } from '@nestjs/passport';
import { ProductsModule } from './products/products.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [config],
      isGlobal: true,
      envFilePath: '../.env',
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      database: config().db.database,
      host: config().db.host,
      username: config().db.username,
      password: config().db.password,
      port: config().db.port,
      entities: config().db.entities,
    }),
    UsersModule,
    AuthModule,
    ProductsModule,
    PassportModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
