const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

export class Validation {
  static async validateEmail(mail: string): Promise<boolean> {
    return emailPattern.test(mail);
  }
}
